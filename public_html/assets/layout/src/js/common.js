$(function () {
    // Новинки и популярные на главной
    var choice = new Swiper('#choice.swiper-container', {
        slidesPerView: 4,
        spaceBetween: 34,
        slidesPerGroup: 1,
        loop: true,
        loopFillGroupWithBlank: true,
        pagination: {
            el: '.swiper-pagination',
            dynamicBullets: true,
            clickable: true,
        },
        breakpoints: {
            1024: {
                slidesPerView: 3,
                spaceBetween: 40,
            },
            768: {
                slidesPerView: 2,
                spaceBetween: 30,
            },
            640: {
                slidesPerView: 2,
                spaceBetween: 20,
            },
            320: {
                slidesPerView: 1,
                spaceBetween: 10,
            }
        }
    });

    // В карточке товара
    var galleryThumbs = new Swiper('.gallery-thumbs', {
        direction: 'vertical',
        spaceBetween: 10,
        slidesPerView: 5,
        //loop: true,
        freeMode: true,
        //loopedSlides: 5, //looped slides should be the same
        watchSlidesVisibility: true,
        watchSlidesProgress: true,
    });
    var galleryTop = new Swiper('.gallery-top', {
        spaceBetween: 10,
        loop:true,
        loopedSlides: 5, //looped slides should be the same
        effect: 'fade',
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
        thumbs: {
            swiper: galleryThumbs,
        },
    });

    // Пагинация
    var $active = $('.page-item.active');
    var $next = $active.next();
    if ($active.length === 0 || $next.hasClass('disabled')) {
        $('.mse2_pag').hide();
    }
    // Ловим событие обновления фильтров для скрытия или отображения кнопки "Загрузить ещё"
    $(document).on('mse2_load', function(e, data) {
        var active = $('.page-item.active');
        var next = active.next();
        if (next.hasClass('disabled')) {
            $('.mse2_pag').hide();
        } else {
            $('.mse2_pag').show();
        }
    });
    // Действия по кнопке загрузить еще
    $('.mse2_pag').on('click', function (e) {
        mSearch2.addPage();
        var params = mSearch2.getFilters();
        mSearch2.Hash.set(params);
        mSearch2.load(params);
    });


    // Menu
    TopMenu.init();
    // Search
    if (typeof searchConfig != 'undefined'){
        Search.init(searchConfig);
    }
    City.init();
    CatsMenu.init();
    Props.init();
    Quize.init();
});

/**
 * Расширяем String.
 * Метод приведения к верхнему регистру перврй буквы в строке.
 * @returns {string}
 */
String.prototype.firstLetterCaps = function() {
    return this.charAt(0).toUpperCase() + this.slice(1);
}

var TopMenu = {
    sel: {
        parent: '.parent',
        link: '.header__menu .parent > a',
        sub: '.header__menu-sub'
    },
    classes: {
        active: 'active'
    },
    init: function () {
        this.load();
    },
    load: function () {
        var self = this;

        $(this.sel.link).on('click', function (e) {
            e.stopPropagation();
            e.preventDefault();
            var $sub = $(this).parents(self.sel.parent).find(self.sel.sub);
            if ($sub.hasClass(self.classes.active)) {
                self.close();
            }
            else {
                self.show($sub);
            }
        });
    },

    show: function (el) {
        TopMenu.close();
        el.addClass(TopMenu.classes.active);
        $('body').bind('click', TopMenu.close);
    },

    close: function () {
        $(TopMenu.sel.sub).removeClass(TopMenu.classes.active);
        $('body').unbind('click', TopMenu.close);
    }
};

var Search = {
    sel: {
        input: '', // Устанавливается в setup
        autocomplete: '.autocomplete',
        history: '.autocomplete__history',
        tips: '.autocomplete__tips',
        result: '.autocomplete__result',
        item: '.autocomplete__result-link'
    },
    classes: {
        active: 'active'
    },
    templates: {
        item: '<a class="autocomplete__result-link" href="javascript:;" title=""></a>',
        empty: '<p class="autocomplete__result-link empty"></p>'
    },
    setup: function(config) {
        this.config = config;
        this.sel.input = 'input[name="' + config.queryVar + '"]';
        this.queries = this.getOldQueries();
        this.url = '/assets/components/cov/search/action.php';
    },
    init: function (config) {
        this.setup(config);
        this.load();
    },
    load: function () {
        var self = this;

        // Ловим фокус на input
        $(this.sel.input).on('focus', function (e) {
            self.showOtherQueries();
            self.showAutocomplete();
        });
        // ловим событие отправки формы
        $(this.sel.input).parents('form').on('submit', function (e) {
            self.formSubmit($(this));
        });
        // Разбираемся с всплытиями и выбором подсказки
        $('body').on('click', function (e) {
            var $el = $(e.target);
            if ($el.is('input') && $el.attr('name') === self.config.queryVar) {
                return false;
            }
            if ($el.parents(self.sel.autocomplete).length > 0) {
                if ($el.is(self.sel.item) && $el.is('a')) {
                    e.preventDefault();
                    $(self.sel.input).val($el.text()).parents('form').submit();
                }
                return false;
            }
            Search.hideAutocomplete();
        });
        // Ловим ввод пользователя
        $(this.sel.input).on('input', function () {
            var q = $(this).val();
            if (q.length < self.config.minQuery) {
                return false;
            }
            self.showOtherQueries(q);
            self.processHistory(self.searchQueries(q));
        });
    },
    // Получаем запросы из LocalStorage
    getOldQueries: function () {
        var queries = [];
        queries = localStorage.getItem('queries') || queries;
        if (queries.length > 0) {
            queries = JSON.parse(queries);
        }
        return queries;
    },
    // Устанавливаем значение в LocalStorage
    setQueries: function (query) {
        var result = Search.searchQueries(query, true);
        if (result.length === 0) {
            Search.queries.unshift(query);
            localStorage.setItem('queries', JSON.stringify(Search.queries));
        }
    },
    // Ищем значения в LocalStorage
    searchQueries: function (q, strict) {
        var result = [];
        if (strict === undefined) {
            strict = false;
        }
        if (Search.queries.length > 0) {
            result = Search.queries.filter(function (query) {
                if (strict) {
                    return query.toLowerCase() === q.toLowerCase();
                }
                return query.toLowerCase().indexOf(q.toLowerCase()) >= 0;
            });
        }
        return result;
    },
    // Показываем основной блок
    showAutocomplete: function () {
        if (Search.queries.length > 0) {
            Search.processHistory(Search.queries)
        }
        else {
            $(Search.sel.history).hide();
        }
        $(Search.sel.autocomplete).addClass(Search.classes.active);
    },
    // Скрываем основной блок
    hideAutocomplete: function () {
        $(Search.sel.autocomplete).removeClass(Search.classes.active);
    },
    // При отправке формы
    formSubmit: function ($form) {
        var q = $form.find(Search.sel.input).val();
        Search.setQueries(q);
    },
    //
    processHistory: function (queries) {

        var historyResult = $(Search.sel.history).find(Search.sel.result);
        historyResult.empty();

        if (queries.length > 0) {
            queries.forEach(function (item, index, array) {
                if (index < 3) {
                    var q = item.firstLetterCaps();
                    $(Search.templates.item).clone().attr('title', q).text(q).appendTo(historyResult);
                }
            });
        }
        else {
            $(Search.templates.empty).clone().text('Вы ранее это не искали.').appendTo(historyResult);
        }
    },
    //
    showOtherQueries: function (query) {
        var data = {
            'action' : 'getQueries',
            'query' : query == 'undefined' ? '' : query
        };
        Search.send(data, Search.sel.tips);
    },
    //
    send: function (data, context) {
        $.ajax({
            type: "post",
            url: Search.url,
            dataType: 'json',
            context: $(context),
            data: data,
            success: function (result, status, obj) {
                if (result.status === 'success') {
                    var $result = $(this).find(Search.sel.result);
                    $result.empty();
                    if (result.count > 0) {
                        result.data.map(function (item) {
                            $(Search.templates.item).clone().text(item.query).attr('title',item.query).appendTo($result);
                        });
                    }
                    else {
                        $(Search.templates.empty).clone().text('Такого еще никто не искал!').appendTo($result);
                    }
                }
            }
        });
    }
};

var City = {
    sel: {
        btnBase: '.header__city-title,.city-load__chois',
        popup: '.city',
        btnClose: '.city__close'
    },
    classes: {
        active: 'active'
    },
    init: function () {
        this.load();
    },
    load: function () {
        var self = this;

        $(this.sel.btnBase).on('click', function (e) {
            e.preventDefault();
            self.show();
        });
        $(this.sel.btnClose).on('click', function (e) {
            e.preventDefault();
            self.close();
        });
    },
    show: function () {
        $(City.sel.popup).addClass(City.classes.active);
    },
    close: function () {
        $(City.sel.popup).removeClass(City.classes.active);
    }
};

var CatsMenu = {
    sel: {
        wrap: '.catsmenu__all',
        btn: '.catsmenu__btn',
        block: '.catsmenu',
        item: '.catsmenu__item-link',
        subMenu: '.catsmenu__submenu',
        result: '.catsmenu__result'
    },
    classes: {
        active: 'active',
        parent: 'parent'
    },
    data: {
        widthMob: 1024,
        url: '/assets/components/cov/catsmenu/action.php'
    },
    setup: function () {
        this.isMobile = $(window).width() <= this.data.widthMob;
    },
    init: function () {
        this.setup();
        this.load();
    },
    load: function () {
        var self = this;

        $(window).resize(function(){
            var winWidth = $(window).width();
            winWidth <= self.data.widthMob ? self.mobileEvent() : self.desktopEvent();
        });

        this.desktopEvent();

        if (this.isMobile) {
            this.mobileEvent();
        }

    },
    desktopEvent: function () {
        $(CatsMenu.sel.wrap).on('mouseover',function (e) {
            CatsMenu.show();
        });

        $(CatsMenu.sel.wrap).on('mouseout',function (e) {
            CatsMenu.hide();
        });
        $(CatsMenu.sel.item).on('mouseover', function (e) {
            CatsMenu.item($(this));
        })
    },
    mobileEvent: function () {
        $(CatsMenu.sel.wrap).off('mouseover');
        $(CatsMenu.sel.wrap).off('mouseout');
        $(CatsMenu.sel.item).off('mouseover');
    },
    show: function () {
        $(CatsMenu.sel.btn).addClass(CatsMenu.classes.active);
        $(CatsMenu.sel.block).addClass(CatsMenu.classes.active);
    },
    hide: function () {
        $(CatsMenu.sel.btn).removeClass(CatsMenu.classes.active);
        $(CatsMenu.sel.block).removeClass(CatsMenu.classes.active);
    },
    item: function (el) {
        var hasSubMenu = el.parents('li').hasClass(CatsMenu.classes.parent);
        if (!hasSubMenu) {return false;}
        var showedSubMenu = $(CatsMenu.sel.subMenu).length > 0 ? $(CatsMenu.sel.subMenu).data('parent') : null;
        if (showedSubMenu === null) {return false;}
        var resource = el.data('id');
        if (resource === showedSubMenu) {return false;}
        CatsMenu.send({
            'action': 'getSubmenuItems',
            'id' : resource
        }, CatsMenu.sel.result)
    },
    send: function (data, context) {
        $.ajax({
            type: "post",
            url: CatsMenu.data.url,
            dataType: 'json',
            context: $(context),
            data: data,
            success: function (result, status, obj) {
                if (result.status === 'success') {
                    this.empty().append(result.data);
                }
            }
        });
    }
};

// Характеристики в карточке товара
var Props = {
    sel: {
        btnToProps:     '.product__base-info-proplink',
        tabs:           '.product__dop',
        btnTab:         '.product__dop-title-link',
        btnWrapTab:     '.product__dop-title-item',
        contTabs:       '.product__dop-content',
        titleTabs:      '.product__dop-title',
        btnDescOption:  '.product__option-desc'
    },
    classes: {
        active:         'active'
    },
    data: {
        tabs: {
            prop: '#prod_options'
        }
    },
    init: function () {
        this.load();
    },
    load: function () {
        var self = this;

        $(document).on('click', this.sel.btnToProps, function (e) {
            e.preventDefault();
            self.chTab(self.data.tabs.prop);
            self.slide(Props.sel.tabs);
        });

        $(document).on('click', this.sel.btnTab, function (e) {
            e.preventDefault();
            self.chTab($(this).attr('href'));
        });

        $(document).on('click', this.sel.btnDescOption, function (e) {
            e.preventDefault();
            var text = $(this).data('text');
            console.log(text);
        });
    },
    slide: function (el) {
        $("html, body").animate({scrollTop: $(el).offset().top + "px"}, 1000);
    },
    chTab: function (to) {
        var el = $(Props.sel.titleTabs).find('*[href="' + to + '"]');
        $(el).parents(Props.sel.titleTabs).find('.' + Props.classes.active).removeClass(Props.classes.active);
        $(el).parents(Props.sel.btnWrapTab).addClass(Props.classes.active);
        $(Props.sel.contTabs).find('.' + Props.classes.active).removeClass(Props.classes.active);
        $(to).addClass(Props.classes.active);
    }
};

// Опрос
var Quize = {
    sel: {
        btn: '.quize__open',
        block: '.quize',
        close: '.quize__close'
    },
    classes: {
        active: 'active'
    },
    data: {
        default: {
            link: 'javascript:;'
        }
    },
    init: function () {
        this.load();
    },
    load: function () {
        var self = this;

        $(document).on('click', this.sel.btn, function (e) {
            e.preventDefault();
            self.open(self.prepareLink(this));
        });

        $(document).on('click', this.sel.close, function (e) {
            e.preventDefault();
            self.close(this, self.prepareLink(this));
        });
    },
    open: function (block) {
        $(Quize.sel.close).attr('href', block);
        $(block).addClass(Quize.classes.active);
    },
    close: function (el, block) {
        $(block).removeClass(Quize.classes.active);
        $(el).attr('href', Quize.data.default.link);
    },
    prepareLink: function (el) {
        var link = $(el).attr('href');
        if (link[0] === '#' || link[0] === '.') {
            return  link;
        }
        return Quize.sel.block;
    }
};
<?php

ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

$pwd = '';
if (!empty($_SERVER['DOCUMENT_ROOT'])) {
    $pwd = $_SERVER['DOCUMENT_ROOT'] . '/index.php';
}
else {
    $pwd = dirname(dirname(dirname($_SERVER['PWD']))) . '/index.php';
}
// Подключаемся к основному сайту по API
define('MODX_API_MODE', true);
require $pwd;
$modx->getService('error','error.modError');
$modx->setLogLevel(modX::LOG_LEVEL_INFO);
$modx->setLogTarget(XPDO_CLI_MODE ? 'ECHO' : 'HTML');


// Данные с старого сайта
$old_data = array(
    'product_tpl' => '5',
    'gallery' => 'ws.images',
    'domain' => 'https://cov35.ru/'
);


$ch = curl_init();
// GET запрос указывается в строке URL
curl_setopt($ch, CURLOPT_URL, 'https://cov35.ru/assets/components/cov/export.php');
curl_setopt($ch, CURLOPT_HEADER, false);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch, CURLOPT_USERAGENT, 'PHP Bot (http://mysite.ru)');
$data = curl_exec($ch);
curl_close($ch);

$old_cat = $modx->fromJSON($data);

$i = 1;
foreach($old_cat as $item) {

    if ($i <= 10000000000000000000000000) {

        $res = array();
        $res['class_key'] = 'msCategory';
        $res['template'] = 2;
        if ($item['template'] == 5) {
            $res['class_key'] = 'msProduct';
            $res['template'] = 3;
        }
        $res['uri_override'] = '1';
        $res['uri'] = $item['uri'];
        $res['pagetitle'] = $item['pagetitle'];
        $res['longtitle'] = $item['longtitle'];
        $res['published'] = $item['published'];
        $res['content'] = $item['content'];
        $res['parent'] = 2;
        if ($item['template'] == $old_data['product_tpl']) {
            $res['price'] = $item['price'];
            $res['old_price'] = $item['oldprice'];
            $res['favorite'] = $item['favourite'];
            $res['popular'] = $item['popular'];
            $res['new'] = $item['new'];
        }

        $response = $modx->runProcessor('resource/create', $res);

        // Сохраняем TV
        $resource = $response->getObject();
        //var_dump(count($resource));
        if (count($resource) == 0) {continue;}
        $resource = $modx->getObject('modResource', $resource['id']);
        $resource->setTVValue('old_id', $item['id']);
        $resource->setTVValue('old_parent', $item['parent']);
        $resource->setTVValue('seo_key', $item['seo-key']);
        $resource->setTVValue('seo_title', $item['seo-title']);
        $resource->setTVValue('seo_desc', $item['seo-desc']);
        $resource->setTVValue('sortindex', $item['sortindex']);

        if (!empty($item['main-img'])) {
            if (!file_exists(dirname($pwd) . '/' . $item['main-img'])) {
                $file = file_get_contents($old_data['domain'] . $item['main-img']);
                mkdir(dirname(dirname($pwd) . '/'. $item['main-img']), 0777, true);
                file_put_contents(dirname($pwd) . '/' . $item['main-img'], $file);
            }
            $resource->setTVValue('main_img', $item['main-img']);
        }

        $resource->save();

        if ($item['template'] == $old_data['product_tpl'] && !empty($item['ws.images'])) {
            foreach ($item['ws.images'] as $image) {

                // Получаем файл
                $img_file = file_get_contents($old_data['domain'] . $image['image']);

                if ($img_file) {
                    // Получаем имя файла для базы данных
                    $name = !empty($image['set']) ? $image['set'] : $resource->get('pagetitle');

                    // Собираем путь для хранения изображения
                    $path = dirname($pwd) . '/images/old_prod/'. $resource->get('id');

                    // Получаем имя файла
                    $img_file_path = explode('/', $image['image']);
                    $img_name = $img_file_path[(count($img_file_path) - 1)];

                    // Создаем папку для изображений
                    mkdir($path, 0777, true);

                    //var_dump($image);exit;

                    // Создаем файл
                    $image = $path . '/' . $img_name;
                    file_put_contents($path . '/' . $img_name, $img_file);

                    // Добавляем файл в галлерею
                    $resp = $modx->runProcessor('gallery/upload',
                        array('id' => $resource->get('id'), 'name' => $name, 'file' => $image),
                        array('processors_path' => MODX_CORE_PATH.'components/minishop2/processors/mgr/')
                    );
                    if ($resp->isError()) {
                        $modx->log(modX::LOG_LEVEL_ERROR, "Error on upload \"$name\": \n". print_r($resp->getAllErrors(), 1));
                    }
                    else {
                        //$modx->log(modX::LOG_LEVEL_INFO, "Successful upload  \"$name\": \n". print_r($resp->getObject(), 1));
                    }
                }
            }
        }

        //var_dump($resource->get('id'));
    }
    $i++;
}

// Раскидывам все по категориям
processTree(3, $modx);

// Удаляем оригиналы изображений
RDir(dirname($pwd) . '/images/old_prod/');


function processTree($old_parent = 3, $modx) {

    $pdo = $modx->getService('pdoFetch');

    $default = array(
        'parents' => '2',
        'class' => 'modResource',
        'fastMode' => false,
        'limit' => 0,
        'return' => 'data',
        'decodeJSON' => true,
        'includeTVs' => 'old_id,old_parent',
        'tvFilters' => 'old_parent=='.$old_parent,
    );

    $pdo->setConfig($default, true);
    $res = $pdo->run();

    foreach ($res as $item) {
        $old_id = $item['old_id'];
        $old_parent = $item['old_parent'];

        if($old_parent != 3) {
            // Выбрать ресурс где $old_id == $item['old_parent']
            $criteria = $modx->newQuery('modResource');
            $criteria->innerJoin('modTemplateVarResource','TV', 'TV.contentid = modResource.id and TV.tmplvarid=5');
            $criteria->where(
                array(
                    'TV.value' => $item['old_parent']
                )
            );
            //$total = $modx->getCount('modResource',$criteria);
            $parent = $modx->getObject('modResource',$criteria);
            // Получить его реальный id
            $parent_id = $parent->id;
            // В $item['parent'] записать id
            $resource = $modx->getObject('modResource', $item['id']);
            $resource->set('parent', $parent_id);
            $resource->save();
        }

        processTree($old_id,$modx);

    }

}

function RDir( $path ) {
    // если путь существует и это папка
    if ( file_exists( $path ) AND is_dir( $path ) ) {
        // открываем папку
        $dir = opendir($path);
        while ( false !== ( $element = readdir( $dir ) ) ) {
            // удаляем только содержимое папки
            if ( $element != '.' AND $element != '..' )  {
                $tmp = $path . '/' . $element;
                chmod( $tmp, 0777 );
                // если элемент является папкой, то
                // удаляем его используя нашу функцию RDir
                if ( is_dir( $tmp ) ) {
                    RDir( $tmp );
                    // если элемент является файлом, то удаляем файл
                } else {
                    unlink( $tmp );
                }
            }
        }
        // закрываем папку
        closedir($dir);
        // удаляем саму папку
        if ( file_exists( $path ) ) {
            rmdir( $path );
        }
    }
}
<?php
define('MODX_API_MODE', true);
require_once dirname(dirname(dirname(dirname(dirname(__FILE__))))) . '/index.php';

$modx->getService('error', 'error.modError');
$modx->setLogLevel(modX::LOG_LEVEL_ERROR);
$modx->setLogTarget('FILE');

if (empty($_SERVER['HTTP_X_REQUESTED_WITH']) || $_SERVER['HTTP_X_REQUESTED_WITH'] != 'XMLHttpRequest') {
    die('Access denied');
}

if (!isset($_REQUEST['action']) || empty($_REQUEST['action'])) {
    die('Access denied');
}

require_once MODX_CORE_PATH . 'components/cov/catsmenu/CatsMenu.php';
$catsmenu = new CatsMenu($modx);

if (!method_exists($catsmenu, $_REQUEST['action'])) {
    exit(false);
}
$result = $catsmenu->{$_REQUEST['action']}();

echo $modx->toJSON($result);

@session_write_close();
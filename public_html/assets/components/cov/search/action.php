<?php
define('MODX_API_MODE', true);
require_once dirname(dirname(dirname(dirname(dirname(__FILE__))))) . '/index.php';

$modx->getService('error', 'error.modError');
$modx->setLogLevel(modX::LOG_LEVEL_ERROR);
$modx->setLogTarget('FILE');

if (empty($_SERVER['HTTP_X_REQUESTED_WITH']) || $_SERVER['HTTP_X_REQUESTED_WITH'] != 'XMLHttpRequest') {
    return false;
}

if (!isset($_REQUEST['action']) || empty($_REQUEST['action'])) {
    return false;
}

require_once MODX_CORE_PATH . 'components/cov/search/Search.php';
$search = new Search($modx);

if (!method_exists($search, $_REQUEST['action'])) {
    exit(false);
}
$result = $search->{$_REQUEST['action']}();

echo $modx->toJSON($result);

@session_write_close();
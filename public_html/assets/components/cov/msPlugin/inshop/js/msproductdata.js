miniShop2.combo.InShop = function (config) {
    config = config || {};
    Ext.applyIf(config, {
        xtype: 'superboxselect',
        allowBlank: true,
        msgTarget: 'under',
        allowAddNewData: false,
        addNewDataOnBlur: true,
        pinList: false,
        resizable: true,
        name: config.name + '[]',
        hiddenName: config.name + '[]',
        anchor: '100%',
        minChars: 0,
        store: new Ext.data.JsonStore({
            id: config.name+'-store',
            root: 'results',
            autoLoad: true,
            autoSave: false,
            totalProperty: 'total',
            fields: ['pagetitle', 'id'],
            url: '/assets/components/cov/connector.php',
            baseParams: {
                action: 'mgr/shops/getlist',
                combo: true,
                parent: '33173'
            }
        }),
        mode: 'remote',
        displayField: 'pagetitle',
        valueField: 'id',
        triggerAction: 'all',
        extraItemCls: 'x-tag',
        expandBtnCls: 'x-form-trigger',
        clearBtnCls: 'x-form-trigger',
        listeners: { // - Не отрабатывает. Не понимаю почему(((
            render: {
                fn: function (data) {
                    console.log('Show:', data);
                    /*Ext.getCmp('modx-panel-resource').getForm().setValues({'inshop[]':['33174','33175']})
                    MODx.fireResourceFormChange();*/
                }
            }
        }
    });

    miniShop2.combo.InShop.superclass.constructor.call(this, config);
};
Ext.extend(miniShop2.combo.InShop, Ext.ux.form.SuperBoxSelect);
Ext.reg('minishop2-combo-inshop', miniShop2.combo.InShop);

/**
 * Для дебага:
 *
 * В итоге данные подставляются в плагине ShopsFild
 *
 * Ext.getCmp('modx-panel-resource').getForm().setValues({'inshop[]':['33174','33175']})
 * Ext.getCmp('modx-panel-resource').getForm().findField('inshop[]')
 * Ext.getCmp('modx-resource-inshop').value
 */

miniShop2.plugin.inshop = {
    getFields: function () {
        return {
            inshop: {
                xtype: 'minishop2-combo-inshop',
                description: '<b>[[+inshop]]</b><br />' + _('ms2_product_inshop_help')
            }
        }
    },
    getColumns: function () {
        return {
            inshop: {
                width: 50,
                sortable: false,
                editor: {
                    xtype: 'minishop2-combo-inshop',
                    name: 'inshop'
                }
            }
        }
    }
};

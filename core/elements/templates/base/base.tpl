<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{$_modx->resource.longtitle ?: $_modx->resource.pagetitle}</title>
    <base href="{'site_url' | config}">

    {block 'styles'}
        <link rel="stylesheet" href="{'assets_url' | config}layout/dist/css/main.min.css">
    {/block}

</head>
<body>

    <header class="header">
        <div class="header__line-first">
            <div class="container">
                <div class="header__city">
                    <a href="javascript:;" class="header__city-title">Вологда</a>
                    <div class="city-load">
                        <div class="city__title-wrap city-load__title">
                            <p class="city__title-link">Вологда</p>
                        </div>
                        <div class="city-load__ctrl">
                            <a href="javascript:;" class="products__item-btn btn city-load__chois">Выбрать</a>
                            <a href="javascript:;" class="products__item-btn btn --blue">Верно</a>
                        </div>
                    </div>
                </div>
                <nav class="header__menu">
                    {var $topMenuParents = '33173,33177,33178,33179,33184'}
                    {var $topMenuItems = '!pdoResources' | snippet : [
                        'parents' => $topMenuParents,
                        'returnIds' => '1'
                    ]}
                    {set $topMenuItems = $topMenuItems ~ ',' ~ $topMenuParents}
                    {'!pdoMenu' | snippet : [
                        'parents' => '0',
                        'resources' => $topMenuItems,
                        'showHidden' => '1',
                        'firstClass' => '',
                        'lastClass' => '',
                        'rowClass' => 'header__menu-item',
                        'parentClass' => 'parent',
                        'tplOuter' => '@INLINE <ul class="header__menu-list">{$wrapper}</ul>',
                        'tplInner' => '@INLINE <ul class="header__menu-sub">{$wrapper}</ul>',
                        'tpl' => '@INLINE <li {$classes}><a href="{$link}" class="header__menu-item-link">{$menutitle}</a>{$wrapper}</li>',
                        'tplInnerRow' => '@INLINE <li class="header__menu-sub-item {$classnames}"><a href="{$link}" class="header__menu-sub-item-link">{$menutitle}</a></li>'
                    ]}
                </nav>
            </div>
        </div>
        <div class="header__line-two">
            <div class="container">
                <div class="header__logo">
                    <a href="{'site_url' | config}" class="header__logo-link">
                        <img src="{'assets_url' | config}images/logo.png" alt="{'site_name' | config}" class="header__logo-img">
                    </a>
                </div>
                <div class="header__search">
                    {'!mSearchFormFix' | snippet : [
                        'tplForm' => '@FILE chanks/forms/search.tpl',
                        'pageId' => '18916',
                        'autocomplete' => '0'
                    ]}
                </div>
                <div class="header__phone">
                    <a href="tel:{('phone1' | config) | preg_replace : '/[^0-9+]/': ''}" class="header__phone-item">{'phone1' | config}</a>
                    <a href="tel:{('phone2' | config) | preg_replace : '/[^0-9+]/': ''}" class="header__phone-item">{'phone2' | config}</a>
                </div>
                <div class="header__account">
                    <a href="javascript:;" class="header__account-register">Регистрация</a>
                    <a href="javascript:;" class="header__account-login">Войти</a>
                </div>
                <div class="header__cart">
                    <a href="{'18917' | url}" class="header__cart-link"></a>
                </div>
            </div>
            {* Autocompleat for search *}
            <div class="header__search-autocomplete autocomplete">
                <div class="container">
                    <div class="autocomplete__left">
                        <div class="autocomplete__history">
                            <div class="autocomplete__title">
                                <p>история поиска</p>
                            </div>
                            <div class="autocomplete__result"></div>
                        </div>
                        <div class="autocomplete__tips">
                            <div class="autocomplete__title">
                                <p>товары<br />часто ищут</p>
                            </div>
                            <div class="autocomplete__result"></div>
                        </div>
                    </div>
                    <div class="autocomplete__right">
                        <div class="autocomplete__img">
                            <a class="autocomplete__link" title="Перйти в раздел" href="javascript:;">
                                <img src="{'assets_url' | config}images/banner2.png" alt="Изображение раздела">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="header__line-thirty">
            <div class="container">
                <nav class="header__cats">
                    <div class="catsmenu__all">
                        <a href="{'2' | url}" class="header__cats-btn catsmenu__btn">
                            <span class="header__cats-icon catsmenu__btn-icon">
                                <span></span>
                                <span></span>
                                <span></span>
                            </span>
                            <span class="header__cats-text">{'2' | resource : 'menutitle'}</span>
                        </a>
                        <div class="catsmenu">
                            <div class="container">
                                <div class="catsmenu__left">
                                    {'!pdoMenu' | snippet : [
                                        'parents' => '2',
                                        'where' => '{"class_key":"msCategory"}',
                                        'level' => '2',
                                        'firstClass' => '',
                                        'lastClass' => '',
                                        'outerClass' => 'catsmenu__list',
                                        'parentClass' => 'parent',
                                        'rowClass' => 'catsmenu__item',
                                        'tplOuter' => '@INLINE <ul {$classes}>{$wrapper}</ul>',
                                        'tpl' => '@INLINE <li {$classes}><a href="{$link}" data-id="{$id}" class="catsmenu__item-link" {$attributes}><img src="{"assets_url" | config}uploads/images/cats/{$alias}.png">{$menutitle ?: $pagetitle}</a></li>'
                                    ]}
                                </div>
                                <div class="catsmenu__right">
                                    <div class="catsmenu__result">
                                        {'!pdoResources' | snippet : [
                                            'parents' => '2',
                                            'limit' => '1',
                                            'sortby' => '{"menuindex":"ASC"}',
                                            'where' => '{"class_key":"msCategory"}',
                                            'tpl' => '@FILE chanks/menu/catsmenu.tpl'
                                        ]}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {'!pdoMenu' | snippet : [
                        'parents' => '2',
                        'limit' => '6',
                        'level' => '1',
                        'firstClass' => '',
                        'lastClass' => '',
                        'rowClass' => 'header__cats-item',
                        'tplOuter' => '@INLINE <ul class="header__cats-list">{$wrapper}</ul>',
                        'tpl' => '@INLINE <li {$classes}><a href="{$link}" class="header__cats-item-link">{$menutitle}</a></li>'
                    ]}
                </nav>
            </div>
        </div>
    </header>

    <div class="template">
        {block 'template'}{/block}
    </div>

    <footer class="footer">
        <div class="container">
            <div class="footer__list">
                <p class="footer__title">Компания</p>
                {'!pdoMenu' | snippet : [
                    'parents' => '0',
                    'resources' => '33180,33178,33181,33179,33182,33183',
                    'showHidden' => '1',
                    'firstClass' => '',
                    'lastClass' => '',
                    'rowClass' => 'footer__list-item',
                    'tplOuter' => '@INLINE {$wrapper}',
                    'tpl' => '@INLINE <a href="{$link}" {$classes}>{$menutitle}</a>'
                ]}
            </div>
            <div class="footer__list">
                <p class="footer__title">Покупателям</p>
                {'!pdoMenu' | snippet : [
                    'parents' => '0',
                    'resources' => '33184,33185,33186',
                    'showHidden' => '1',
                    'firstClass' => '',
                    'lastClass' => '',
                    'rowClass' => 'footer__list-item',
                    'tplOuter' => '@INLINE {$wrapper}',
                    'tpl' => '@INLINE <a href="{$link}" {$classes}>{$menutitle}</a>'
                ]}
            </div>
            <div class="footer__list">
                <p class="footer__title">Помощь</p>
                {'!pdoMenu' | snippet : [
                    'parents' => '0',
                    'resources' => '33187,33188,33189',
                    'showHidden' => '1',
                    'firstClass' => '',
                    'lastClass' => '',
                    'rowClass' => 'footer__list-item',
                    'tplOuter' => '@INLINE {$wrapper}',
                    'tpl' => '@INLINE <a href="{$link}" {$classes}>{$menutitle}</a>'
                ]}
            </div>
            <div class="footer__price">
                <p class="footer__title">Прайс листы</p>
                {'@FILE snippets/getPrices.php' | snippet : [
                    'tpl' => '@FILE chanks/prices/item.tpl'
                ]}
                <div class="footer__price-list">
                    <img src="{'assets_url' | config}images/visa-inverted_82058.png" alt="Visa" class="footer__price-list-item">
                    <img src="{'assets_url' | config}images/visa-inverted_82058.png" alt="Visa" class="footer__price-list-item">
                    <img src="{'assets_url' | config}images/visa-inverted_82058.png" alt="Visa" class="footer__price-list-item">
                    <img src="{'assets_url' | config}images/visa-inverted_82058.png" alt="Visa" class="footer__price-list-item">
                    <img src="{'assets_url' | config}images/visa-inverted_82058.png" alt="Visa" class="footer__price-list-item">
                    <img src="{'assets_url' | config}images/visa-inverted_82058.png" alt="Visa" class="footer__price-list-item">
                    <img src="{'assets_url' | config}images/visa-inverted_82058.png" alt="Visa" class="footer__price-list-item">
                    <img src="{'assets_url' | config}images/visa-inverted_82058.png" alt="Visa" class="footer__price-list-item">
                </div>
            </div>
            <div class="footer__contact">
                <p class="footer__title">Оставайтесь на связи</p>
                <div class="footer__social">
                    {if 'vk' | config}
                        <a href="{'vk' | config}" target="_blank" class="footer__social-link">
                            <img src="{'assets_url' | config}images/vk.png" alt="vk" class="footer__social-img">
                        </a>
                    {/if}
                </div>
                <div class="footer__phone">
                    <a href="tel:{('phone1' | config) | preg_replace : '/[^0-9+]/': ''}" class="footer__phone-link">{'phone1' | config}</a>
                </div>
                <div class="footer__address">
                    <a href="{'33173' | url}" target="_blank" class="footer__address-link">Адреса магазинов в г. Вологда</a>
                </div>
            </div>
            <div class="footer__copyright">
                <p class="footer__copyright-text">Администрация Сайта не несет ответственность за размещение Пользователями материалов (в т.ч. информации и изображений), их содержание, качество</p>
                <p class="footer__copyright-date">с 2002 - 2019</p>
            </div>
        </div>
    </footer>

    {block 'popups'}
        <div class="city" id="city">
            <div class="city__content">
                <div class="city__title">
                    <p class="city__title-text">Ваш населенный пункт</p>
                    <div class="city__title-wrap">
                        <p class="city__title-link">Вологда</p>
                        <span class="city__title-shops">3 специализированных магазина</span>
                    </div>
                </div>
                <div class="city__maplink">
                    <a href="javascript:;" class="city__maplink-link">Посмотреть на карте</a href="javascript:;">
                </div>
                <form action="/" method="post" class="city__search">
                    <input class="city__search-input" type="text" name="city" placeholder="Город">
                    <input class="city__search-submit" type="submit" value="">
                </form>
                <ul class="city__list">
                    <li class="city__item">
                        <a href="javascript:;" class="city__item-link">Москва</a>
                    </li>
                    <li class="city__item">
                        <a href="javascript:;" class="city__item-link">Санкт-петербург</a>
                    </li>
                    <li class="city__item">
                        <a href="javascript:;" class="city__item-link">Череповец</a>
                    </li>
                    <li class="city__item">
                        <a href="javascript:;" class="city__item-link">Ярославль</a>
                    </li>
                    <li class="city__item">
                        <a href="javascript:;" class="city__item-link">Чагода</a>
                    </li>
                    <li class="city__item">
                        <a href="javascript:;" class="city__item-link">Тихвин</a>
                    </li>
                    <li class="city__item">
                        <a href="javascript:;" class="city__item-link">Мурманск</a>
                    </li>
                    <li class="city__item">
                        <a href="javascript:;" class="city__item-link">Вельск</a>
                    </li>
                    <li class="city__item">
                        <a href="javascript:;" class="city__item-link">Нижний Новгород</a>
                    </li>
                    <li class="city__item">
                        <a href="javascript:;" class="city__item-link">Москва</a>
                    </li>
                    <li class="city__item">
                        <a href="javascript:;" class="city__item-link">Санкт-петербург</a>
                    </li>
                    <li class="city__item">
                        <a href="javascript:;" class="city__item-link">Череповец</a>
                    </li>
                    <li class="city__item">
                        <a href="javascript:;" class="city__item-link">Ярославль</a>
                    </li>
                    <li class="city__item">
                        <a href="javascript:;" class="city__item-link">Чагода</a>
                    </li>
                    <li class="city__item">
                        <a href="javascript:;" class="city__item-link">Тихвин</a>
                    </li>
                    <li class="city__item">
                        <a href="javascript:;" class="city__item-link">Мурманск</a>
                    </li>
                    <li class="city__item">
                        <a href="javascript:;" class="city__item-link">Вельск</a>
                    </li>
                    <li class="city__item">
                        <a href="javascript:;" class="city__item-link">Нижний Новгород</a>
                    </li>
                    <li class="city__item">
                        <a href="javascript:;" class="city__item-link">Москва</a>
                    </li>
                    <li class="city__item">
                        <a href="javascript:;" class="city__item-link">Санкт-петербург</a>
                    </li>
                    <li class="city__item">
                        <a href="javascript:;" class="city__item-link">Череповец</a>
                    </li>
                    <li class="city__item">
                        <a href="javascript:;" class="city__item-link">Ярославль</a>
                    </li>
                    <li class="city__item">
                        <a href="javascript:;" class="city__item-link">Чагода</a>
                    </li>
                    <li class="city__item">
                        <a href="javascript:;" class="city__item-link">Тихвин</a>
                    </li>
                    <li class="city__item">
                        <a href="javascript:;" class="city__item-link">Мурманск</a>
                    </li>
                    <li class="city__item">
                        <a href="javascript:;" class="city__item-link">Вельск</a>
                    </li>
                    <li class="city__item">
                        <a href="javascript:;" class="city__item-link">Нижний Новгород</a>
                    </li>
                </ul>
                <a class="city__close" href="javascript:;"></a>
            </div>
        </div>
        <div class="auth"></div>
    {/block}

    {block 'scripts'}
        <script src="{'assets_url' | config}layout/dist/js/scripts.min.js"></script>
    {/block}
</body>
</html>
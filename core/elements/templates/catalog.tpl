{extends 'file:templates/base/base.tpl'}

{block 'template'}

    {include 'file:chanks/base/banners.tpl'}

    <div class="template__block">

        {include 'file:chanks/base/breadcrumbs.tpl'}

        {'!mFilter2' | snippet : [
            'element' => 'msProducts',
            'limit' => '12',
            'tplOuter' => '@FILE chanks/filters/mFilter2Outer.tpl',
            'tpls' => '@FILE chanks/product/listItem.tpl',
            'parents' => $_modx->resource.id,
            'includeThumbs' => 'list',
            'showEmptyFilters' => '0',
            'where' => '{"Data.price:>":"0"}',
            'ajax' => '1',
            'ajaxHistory' => '1',
            'ajaxMode' => 'default',
            'sort' => 'ms|price',
            'aliases' => 'resource|parent==parents,ms|price==price',
            'filters' => 'ms|price:number,resource|parent:categories',
            'tplFilter.outer.price' => '@FILE chanks/filters/outer.price.tpl',
            'tplFilter.row.price' => '@FILE chanks/filters/row.price.tpl',
            'tplFilter.outer.parents' => '@FILE chanks/filters/outer.parents.tpl',
            'tplFilter.row.parents' => '@FILE chanks/filters/row.parents.tpl'
        ]}

    </div>

{/block}
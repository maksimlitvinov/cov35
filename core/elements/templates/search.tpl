{extends 'file:templates/base/base.tpl'}

{block 'template'}
    <div class="template__block">

        {include 'file:chanks/base/breadcrumbs.tpl'}

        {include 'file:chanks/base/title.tpl'}

        <div class="search">
            {var $pids = '!mSearch2' | snippet : [
                'returnIds' => '1',
                'limit' => '0',
                'where' => '{"class_key:=":"msProduct"}'
            ]}
            {var $result = '!pdoPage' | snippet : [
                'element' => 'msProducts',
                'parents' => '2',
                'limit' => '16',
                'resources' => $pids ?: '9999999999999999999999999999999999999999999',
                'sortby' => 'ids',
                'ajax' => '1',
                'ajaxHistory' => '1',
                'ajaxMode' => 'default',
                'where' => '{"Data.price:>":"0"}',
                'tpl' => '@FILE chanks/product/listItem.tpl'
            ]}
            {if $_modx->getPlaceholder('page.total') != 0}
                <div class="search__title mb-20">
                    <p class="h1">По запросу "{$.get['query']}" найдено {$_modx->getPlaceholder('page.total')} товаров:</p>
                </div>
                <div class="catalog">
                    <div class="products">
                        <div class="products__list">
                            {$result}
                        </div>
                        <div class="products__more">
                            {$_modx->getPlaceholder('page.nav')}
                        </div>
                    </div>
                </div>
            {else}
                <div class="search__title">
                    <p class="h1">По запросу "{$.get['query']}" ничего не найдено.</p>
                </div>
            {/if}
        </div>

    </div>
{/block}
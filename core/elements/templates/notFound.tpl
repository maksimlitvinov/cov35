{extends 'file:templates/base/base.tpl'}

{block 'template'}

    {include 'file:chanks/base/banners.tpl' doc='1'}

    <div class="not_found">
        <div class="container ">
            <div class="products__more">
                {include 'file:chanks/base/title.tpl'}
                <a href="{'1' | url}" class="products__more-btn">Перейти на главную</a>
            </div>
        </div>

        {var $fav = '!msProducts' | snippet : [
            'parents' => '2',
            'limit' => '12',
            'includeThumbs' => 'list',
            'where' => '{"Data.favorite:>":"0","Data.price:>":"0"}',
            'sortby' => '{"price":"DESC"}',
            'tpl' => '@FILE chanks/product/listItemSlider.tpl'
        ]}
        {if $fav ?}
            <div class="container">
                <div class="products">
                    <p class="products__title --h1">Выбор покупателей</p>
                    <div id="choice" class="swiper-container">
                        <div class="products__list swiper-wrapper">
                            {$fav}
                            {*<div class="products__item swiper-slide">
                                <a href="javascript:;" class="products__item-link">
                                    <div class="products__item-block">
                                        <img src="{'assets_url' | config}images/product.jpg" alt="" class="product__item-img">
                                    </div>
                                    <div class="products__item-block">
                                        <p class="products__item-title">Газовый котел BAXI LUNA-3 240 Fi</p>
                                        <p class="products__item-desc">газовый конвекционный котел мощность 25 кВт, двухконтурный КПД 92.9 %, закрытая камера сгорания</p>
                                    </div>
                                    <div class="products__item-block --price-block">
                                        <span class="products__item-sale">- 10%</span>
                                        <div class="products__item-prices">
                                            <span class="products__item-price">54 400 р.</span>
                                            <span class="products__item-price --old">59 999 р.</span>
                                        </div>
                                    </div>
                                </a>
                                <form class="products__item-btns">
                                    <input type="hidden" name="count" value="1">
                                    <input type="hidden" name="id" value="1">
                                    <input type="hidden" name="options" value="[]">
                                    <a href="javascript:;" class="products__item-btn btn">Купить в 1 клик</a>
                                    <input type="submit" name="submit" value="Купить" class="products__item-btn --red">
                                </form>
                                <p class="products__item-available">наличие:
                                    <a class="products__item-available-link" href="javascript:;">3 магазина</a>
                                </p>
                            </div>
                            <div class="products__item swiper-slide">
                                <a href="javascript:;" class="products__item-link">
                                    <div class="products__item-block">
                                        <img src="{'assets_url' | config}images/product.jpg" alt="" class="product__item-img">
                                    </div>
                                    <div class="products__item-block">
                                        <p class="products__item-title">Газовый котел BAXI LUNA-3 240 Fi</p>
                                        <p class="products__item-desc">газовый конвекционный котел мощность 25 кВт, двухконтурный КПД 92.9 %, закрытая камера сгорания</p>
                                    </div>
                                    <div class="products__item-block --price-block">
                                        <span class="products__item-sale">- 10%</span>
                                        <div class="products__item-prices">
                                            <span class="products__item-price">54 400 р.</span>
                                            <span class="products__item-price --old">59 999 р.</span>
                                        </div>
                                    </div>
                                </a>
                                <form class="products__item-btns">
                                    <input type="hidden" name="count" value="1">
                                    <input type="hidden" name="id" value="1">
                                    <input type="hidden" name="options" value="[]">
                                    <a href="javascript:;" class="products__item-btn btn">Купить в 1 клик</a>
                                    <input type="submit" name="submit" value="Купить" class="products__item-btn --red">
                                </form>
                                <p class="products__item-available">наличие:
                                    <a class="products__item-available-link" href="javascript:;">3 магазина</a>
                                </p>
                            </div>
                            <div class="products__item swiper-slide">
                                <a href="javascript:;" class="products__item-link">
                                    <div class="products__item-block">
                                        <img src="{'assets_url' | config}images/product.jpg" alt="" class="product__item-img">
                                    </div>
                                    <div class="products__item-block">
                                        <p class="products__item-title">Газовый котел BAXI LUNA-3 240 Fi</p>
                                        <p class="products__item-desc">газовый конвекционный котел мощность 25 кВт, двухконтурный КПД 92.9 %, закрытая камера сгорания</p>
                                    </div>
                                    <div class="products__item-block --price-block">
                                        <span class="products__item-sale">- 10%</span>
                                        <div class="products__item-prices">
                                            <span class="products__item-price">54 400 р.</span>
                                            <span class="products__item-price --old">59 999 р.</span>
                                        </div>
                                    </div>
                                </a>
                                <form class="products__item-btns">
                                    <input type="hidden" name="count" value="1">
                                    <input type="hidden" name="id" value="1">
                                    <input type="hidden" name="options" value="[]">
                                    <a href="javascript:;" class="products__item-btn btn">Купить в 1 клик</a>
                                    <input type="submit" name="submit" value="Купить" class="products__item-btn --red">
                                </form>
                                <p class="products__item-available">наличие:
                                    <a class="products__item-available-link" href="javascript:;">3 магазина</a>
                                </p>
                            </div>
                            <div class="products__item swiper-slide">
                                <a href="javascript:;" class="products__item-link">
                                    <div class="products__item-block">
                                        <img src="{'assets_url' | config}images/product.jpg" alt="" class="product__item-img">
                                    </div>
                                    <div class="products__item-block">
                                        <p class="products__item-title">Газовый котел BAXI LUNA-3 240 Fi</p>
                                        <p class="products__item-desc">газовый конвекционный котел мощность 25 кВт, двухконтурный КПД 92.9 %, закрытая камера сгорания</p>
                                    </div>
                                    <div class="products__item-block --price-block">
                                        <span class="products__item-sale">- 10%</span>
                                        <div class="products__item-prices">
                                            <span class="products__item-price">54 400 р.</span>
                                            <span class="products__item-price --old">59 999 р.</span>
                                        </div>
                                    </div>
                                </a>
                                <form class="products__item-btns">
                                    <input type="hidden" name="count" value="1">
                                    <input type="hidden" name="id" value="1">
                                    <input type="hidden" name="options" value="[]">
                                    <a href="javascript:;" class="products__item-btn btn">Купить в 1 клик</a>
                                    <input type="submit" name="submit" value="Купить" class="products__item-btn --red">
                                </form>
                                <p class="products__item-available">наличие:
                                    <a class="products__item-available-link" href="javascript:;">3 магазина</a>
                                </p>
                            </div>
                            <div class="products__item swiper-slide">
                                <a href="javascript:;" class="products__item-link">
                                    <div class="products__item-block">
                                        <img src="{'assets_url' | config}images/product.jpg" alt="" class="product__item-img">
                                    </div>
                                    <div class="products__item-block">
                                        <p class="products__item-title">Газовый котел BAXI LUNA-3 240 Fi</p>
                                        <p class="products__item-desc">газовый конвекционный котел мощность 25 кВт, двухконтурный КПД 92.9 %, закрытая камера сгорания</p>
                                    </div>
                                    <div class="products__item-block --price-block">
                                        <span class="products__item-sale">- 10%</span>
                                        <div class="products__item-prices">
                                            <span class="products__item-price">54 400 р.</span>
                                            <span class="products__item-price --old">59 999 р.</span>
                                        </div>
                                    </div>
                                </a>
                                <form class="products__item-btns">
                                    <input type="hidden" name="count" value="1">
                                    <input type="hidden" name="id" value="1">
                                    <input type="hidden" name="options" value="[]">
                                    <a href="javascript:;" class="products__item-btn btn">Купить в 1 клик</a>
                                    <input type="submit" name="submit" value="Купить" class="products__item-btn --red">
                                </form>
                                <p class="products__item-available">наличие:
                                    <a class="products__item-available-link" href="javascript:;">3 магазина</a>
                                </p>
                            </div>
                            <div class="products__item swiper-slide">
                                <a href="javascript:;" class="products__item-link">
                                    <div class="products__item-block">
                                        <img src="{'assets_url' | config}images/product.jpg" alt="" class="product__item-img">
                                    </div>
                                    <div class="products__item-block">
                                        <p class="products__item-title">Газовый котел BAXI LUNA-3 240 Fi</p>
                                        <p class="products__item-desc">газовый конвекционный котел мощность 25 кВт, двухконтурный КПД 92.9 %, закрытая камера сгорания</p>
                                    </div>
                                    <div class="products__item-block --price-block">
                                        <span class="products__item-sale">- 10%</span>
                                        <div class="products__item-prices">
                                            <span class="products__item-price">54 400 р.</span>
                                            <span class="products__item-price --old">59 999 р.</span>
                                        </div>
                                    </div>
                                </a>
                                <form class="products__item-btns">
                                    <input type="hidden" name="count" value="1">
                                    <input type="hidden" name="id" value="1">
                                    <input type="hidden" name="options" value="[]">
                                    <a href="javascript:;" class="products__item-btn btn">Купить в 1 клик</a>
                                    <input type="submit" name="submit" value="Купить" class="products__item-btn --red">
                                </form>
                                <p class="products__item-available">наличие:
                                    <a class="products__item-available-link" href="javascript:;">3 магазина</a>
                                </p>
                            </div>
                            <div class="products__item swiper-slide">
                                <a href="javascript:;" class="products__item-link">
                                    <div class="products__item-block">
                                        <img src="{'assets_url' | config}images/product.jpg" alt="" class="product__item-img">
                                    </div>
                                    <div class="products__item-block">
                                        <p class="products__item-title">Газовый котел BAXI LUNA-3 240 Fi</p>
                                        <p class="products__item-desc">газовый конвекционный котел мощность 25 кВт, двухконтурный КПД 92.9 %, закрытая камера сгорания</p>
                                    </div>
                                    <div class="products__item-block --price-block">
                                        <span class="products__item-sale">- 10%</span>
                                        <div class="products__item-prices">
                                            <span class="products__item-price">54 400 р.</span>
                                            <span class="products__item-price --old">59 999 р.</span>
                                        </div>
                                    </div>
                                </a>
                                <form class="products__item-btns">
                                    <input type="hidden" name="count" value="1">
                                    <input type="hidden" name="id" value="1">
                                    <input type="hidden" name="options" value="[]">
                                    <a href="javascript:;" class="products__item-btn btn">Купить в 1 клик</a>
                                    <input type="submit" name="submit" value="Купить" class="products__item-btn --red">
                                </form>
                                <p class="products__item-available">наличие:
                                    <a class="products__item-available-link" href="javascript:;">3 магазина</a>
                                </p>
                            </div>
                            <div class="products__item swiper-slide">
                                <a href="javascript:;" class="products__item-link">
                                    <div class="products__item-block">
                                        <img src="{'assets_url' | config}images/product.jpg" alt="" class="product__item-img">
                                    </div>
                                    <div class="products__item-block">
                                        <p class="products__item-title">Газовый котел BAXI LUNA-3 240 Fi</p>
                                        <p class="products__item-desc">газовый конвекционный котел мощность 25 кВт, двухконтурный КПД 92.9 %, закрытая камера сгорания</p>
                                    </div>
                                    <div class="products__item-block --price-block">
                                        <span class="products__item-sale">- 10%</span>
                                        <div class="products__item-prices">
                                            <span class="products__item-price">54 400 р.</span>
                                            <span class="products__item-price --old">59 999 р.</span>
                                        </div>
                                    </div>
                                </a>
                                <form class="products__item-btns">
                                    <input type="hidden" name="count" value="1">
                                    <input type="hidden" name="id" value="1">
                                    <input type="hidden" name="options" value="[]">
                                    <a href="javascript:;" class="products__item-btn btn">Купить в 1 клик</a>
                                    <input type="submit" name="submit" value="Купить" class="products__item-btn --red">
                                </form>
                                <p class="products__item-available">наличие:
                                    <a class="products__item-available-link" href="javascript:;">3 магазина</a>
                                </p>
                            </div>
                            <div class="products__item swiper-slide">
                                <a href="javascript:;" class="products__item-link">
                                    <div class="products__item-block">
                                        <img src="{'assets_url' | config}images/product.jpg" alt="" class="product__item-img">
                                    </div>
                                    <div class="products__item-block">
                                        <p class="products__item-title">Газовый котел BAXI LUNA-3 240 Fi</p>
                                        <p class="products__item-desc">газовый конвекционный котел мощность 25 кВт, двухконтурный КПД 92.9 %, закрытая камера сгорания</p>
                                    </div>
                                    <div class="products__item-block --price-block">
                                        <span class="products__item-sale">- 10%</span>
                                        <div class="products__item-prices">
                                            <span class="products__item-price">54 400 р.</span>
                                            <span class="products__item-price --old">59 999 р.</span>
                                        </div>
                                    </div>
                                </a>
                                <form class="products__item-btns">
                                    <input type="hidden" name="count" value="1">
                                    <input type="hidden" name="id" value="1">
                                    <input type="hidden" name="options" value="[]">
                                    <a href="javascript:;" class="products__item-btn btn">Купить в 1 клик</a>
                                    <input type="submit" name="submit" value="Купить" class="products__item-btn --red">
                                </form>
                                <p class="products__item-available">наличие:
                                    <a class="products__item-available-link" href="javascript:;">3 магазина</a>
                                </p>
                            </div>
                            <div class="products__item swiper-slide">
                                <a href="javascript:;" class="products__item-link">
                                    <div class="products__item-block">
                                        <img src="{'assets_url' | config}images/product.jpg" alt="" class="product__item-img">
                                    </div>
                                    <div class="products__item-block">
                                        <p class="products__item-title">Газовый котел BAXI LUNA-3 240 Fi</p>
                                        <p class="products__item-desc">газовый конвекционный котел мощность 25 кВт, двухконтурный КПД 92.9 %, закрытая камера сгорания</p>
                                    </div>
                                    <div class="products__item-block --price-block">
                                        <span class="products__item-sale">- 10%</span>
                                        <div class="products__item-prices">
                                            <span class="products__item-price">54 400 р.</span>
                                            <span class="products__item-price --old">59 999 р.</span>
                                        </div>
                                    </div>
                                </a>
                                <form class="products__item-btns">
                                    <input type="hidden" name="count" value="1">
                                    <input type="hidden" name="id" value="1">
                                    <input type="hidden" name="options" value="[]">
                                    <a href="javascript:;" class="products__item-btn btn">Купить в 1 клик</a>
                                    <input type="submit" name="submit" value="Купить" class="products__item-btn --red">
                                </form>
                                <p class="products__item-available">наличие:
                                    <a class="products__item-available-link" href="javascript:;">3 магазина</a>
                                </p>
                            </div>
                            <div class="products__item swiper-slide">
                                <a href="javascript:;" class="products__item-link">
                                    <div class="products__item-block">
                                        <img src="{'assets_url' | config}images/product.jpg" alt="" class="product__item-img">
                                    </div>
                                    <div class="products__item-block">
                                        <p class="products__item-title">Газовый котел BAXI LUNA-3 240 Fi</p>
                                        <p class="products__item-desc">газовый конвекционный котел мощность 25 кВт, двухконтурный КПД 92.9 %, закрытая камера сгорания</p>
                                    </div>
                                    <div class="products__item-block --price-block">
                                        <span class="products__item-sale">- 10%</span>
                                        <div class="products__item-prices">
                                            <span class="products__item-price">54 400 р.</span>
                                            <span class="products__item-price --old">59 999 р.</span>
                                        </div>
                                    </div>
                                </a>
                                <form class="products__item-btns">
                                    <input type="hidden" name="count" value="1">
                                    <input type="hidden" name="id" value="1">
                                    <input type="hidden" name="options" value="[]">
                                    <a href="javascript:;" class="products__item-btn btn">Купить в 1 клик</a>
                                    <input type="submit" name="submit" value="Купить" class="products__item-btn --red">
                                </form>
                                <p class="products__item-available">наличие:
                                    <a class="products__item-available-link" href="javascript:;">3 магазина</a>
                                </p>
                            </div>
                            <div class="products__item swiper-slide">
                                <a href="javascript:;" class="products__item-link">
                                    <div class="products__item-block">
                                        <img src="{'assets_url' | config}images/product.jpg" alt="" class="product__item-img">
                                    </div>
                                    <div class="products__item-block">
                                        <p class="products__item-title">Газовый котел BAXI LUNA-3 240 Fi</p>
                                        <p class="products__item-desc">газовый конвекционный котел мощность 25 кВт, двухконтурный КПД 92.9 %, закрытая камера сгорания</p>
                                    </div>
                                    <div class="products__item-block --price-block">
                                        <span class="products__item-sale">- 10%</span>
                                        <div class="products__item-prices">
                                            <span class="products__item-price">54 400 р.</span>
                                            <span class="products__item-price --old">59 999 р.</span>
                                        </div>
                                    </div>
                                </a>
                                <form class="products__item-btns">
                                    <input type="hidden" name="count" value="1">
                                    <input type="hidden" name="id" value="1">
                                    <input type="hidden" name="options" value="[]">
                                    <a href="javascript:;" class="products__item-btn btn">Купить в 1 клик</a>
                                    <input type="submit" name="submit" value="Купить" class="products__item-btn --red">
                                </form>
                                <p class="products__item-available">наличие:
                                    <a class="products__item-available-link" href="javascript:;">3 магазина</a>
                                </p>
                            </div>*}
                        </div>
                        <div class="swiper-pagination"></div>
                    </div>
                    <div class="products__more">
                        <a href="javascript:;" class="products__more-btn">Смотреть все товары</a>
                    </div>
                </div>
            </div>
        {/if}
    </div>
{/block}
{extends 'file:templates/base/base.tpl'}

{block 'template'}
    
        <div class="template__block">

            {include 'file:chanks/base/breadcrumbs.tpl'}

            {include 'file:chanks/base/title.tpl'}

            <div class="content">
                {$_modx->resource.content}
            </div>

        </div>

{/block}
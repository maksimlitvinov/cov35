{extends 'file:templates/base/base.tpl'}

{block 'template'}
    <div class="template__block">

        {include 'file:chanks/base/banners.tpl' doc='1'}

        {include 'file:chanks/base/breadcrumbs.tpl'}

        {include 'file:chanks/base/title.tpl'}

        <div class="product">
            <div class="product__base">
                {if $_modx->resource.article ?}
                    <p class="product__base-article">Артикул {$_modx->resource.article}</p>
                {/if}
                <div class="product__base-img">
                    <div class="product__base-img-prev swiper-container gallery-thumbs">
                        <div class="product__base-img-prev-list swiper-wrapper">
                            {'!msGallery' | snippet : [
                                'tpl' => '@FILE chanks/product/thumb.tpl'
                            ]}
                        </div>
                    </div>
                    <div class="product__base-img-base swiper-container gallery-top">
                        <div class="swiper-wrapper">
                            {'!msGallery' | snippet : [
                                'tpl' => '@FILE chanks/product/gallery.tpl'
                            ]}
                        </div>
                        <div class="swiper-button-next swiper-button-white"></div>
                        <div class="swiper-button-prev swiper-button-white"></div>
                    </div>
                </div>
                <div class="product__base-info">
                    <div class="product__base-info-left">
                        {if $_modx->resource.prod_type ?}
                            <p class="product__base-info-prop">
                                Тип: <span>{$_modx->resource.prod_type}</span>
                            </p>
                        {/if}
                        {if $_modx->resource.prod_count_el_conf ?}
                            <p class="product__base-info-prop">
                                Количество электрических конфорок: <span>{$_modx->resource.prod_count_el_conf}</span>
                            </p>
                        {/if}
                        {if $_modx->resource.prod_control ?}
                            <p class="product__base-info-prop">
                                Управление: <span>{$_modx->resource.prod_control}</span>
                            </p>
                        {/if}
                        {if $_modx->resource.prod_mat_var_poverh ?}
                            <p class="product__base-info-prop">
                                Материал варочной поверхности: <span>{$_modx->resource.prod_mat_var_poverh}</span>
                            </p>
                        {/if}
                        {if $_modx->resource.prod_type_el_con?}
                            <p class="product__base-info-prop">
                                Тип электрических конфорок: <span>{$_modx->resource.prod_type_el_con}</span>
                            </p>
                        {/if}
                        {if $_modx->resource.prod_vol_oven ?}
                            <p class="product__base-info-prop">
                                Объем духовки, в литрах: <span>{$_modx->resource.prod_vol_oven}</span>
                            </p>
                        {/if}
                        {if $_modx->resource.prod_light_oven ?}
                            <p class="product__base-info-prop">
                                Подсветка духовки: <span>{$_modx->resource.prod_light_oven}</span>
                            </p>
                        {/if}
                        {if $_modx->resource.prod_res_heat_ind ?}
                            <p class="product__base-info-prop">
                                Индикатор остаточного тепла: <span>{$_modx->resource.prod_res_heat_ind}</span>
                            </p>
                        {/if}
                        <a class="product__base-info-proplink" href="javascript:;">Все характеристики</a>
                        <p class="product__base-info-prop">
                            Страна производства: RO
                        </p>
                        <p class="product__base-info-prop">
                            Гарантия производителя: 1
                        </p>
                    </div>
                    <div class="product__base-info-right">
                        <div class="product__base-info-block">
                            <p class="product__base-info-price">{$price} <span>p</span></p>
                            {if $old_price ?}
                                {var $sale = ((100 - $_modx->resource.price * 100 / $_modx->resource.old_price) | limit : '2') | replace : '.' : ''}
                                <p class="product__base-info-sale">
                                    <span class="product__base-info-sale-pr">{$sale}%</span>
                                    <span class="product__base-info-sale-num">{$_modx->resource.old_price - $_modx->resource.price}</span>
                                </p>
                            {/if}
                        </div>
                        <form action="{$_modx->resource.uri}" class="product__base-info-block ms2_form">
                            <input type="hidden" name="count" value="1">
                            <input type="hidden" name="options[]">
                            <input type="hidden" name="id" value="{$_modx->resource.id}"/>
                            <button type="submit" class="btn blue w-100" name="ms2_action" value="cart/add">Купить</button>
                        </form>
                        <p class="product__base-info-delivery">Доставка, от 1 <span>p</span></p>
                        <p class="product__base-info-shop">
                            Продавец: <span>М.видио</span>
                            <a href="javascript:;" class="icon-info"></a>
                        </p>
                        <a class="btn btn-default" href="javascript:;">Предложения продовцов</a>
                    </div>
                </div>
            </div>
            <div class="product__dop">
                {var $revs = '!getComments' | snippet : [
                    'outputSeparator' => ',',
                    'tpl' => '@INLINE {$id}'
                ]}
                <ul class="product__dop-title">
                    <li class="product__dop-title-item active">
                        <a href="#prod_desc" class="product__dop-title-link">Описание</a>
                    </li>
                    <li class="product__dop-title-item">
                        <a href="#prod_options" class="product__dop-title-link">Характеристики</a>
                    </li>
                    <li class="product__dop-title-item">
                        <a href="#prod_rev" class="product__dop-title-link">Отзывы <span>{($revs | split) | length}</span></a>
                    </li>
                    <li class="product__dop-title-item">
                        <a href="#prod_comment" class="product__dop-title-link">Коментарии</a>
                    </li>
                    <li class="product__dop-title-item">
                        <a href="#prod_vision" class="product__dop-title-link">Обзоры</a>
                    </li>
                    <li class="product__dop-title-item">
                        <a href="#prod_quest" class="product__dop-title-link">Вопрос ответ <span>3/2</span></a>
                    </li>
                </ul>
                <div class="product__dop-content">
                    <div id="prod_desc" class="product__dop-content-item active">
                        <div class="product__dop-content-block">
                            <div class="content">
                                {$_modx->resource.content}
                            </div>

                            <div class="quize-mini">
                                <p class="quize-mini__title">Сделаем сайт лучше</p>
                                <p class="quize-mini__text">Пожалуйста, ответьте на несколько вопросов</p>
                                <a href="#quize" class="btn orange quize__open">Пройти опрос</a>
                            </div>
                        </div>
                    </div>
                    <div id="prod_options" class="product__dop-content-item">
                        {var $options = '@FILE snippets/getOptions.php' | snippet}
                        {if $options ?}
                        <table class="product__option">
                            {foreach $options as $option}
                                <tr>
                                    <td>
                                        <p class="product__option-wrap">
                                            <span class="product__option-caption">{$option.caption}</span>
                                            {if $option.description ?}
                                                <a href="javascript:;" data-text="{$option.description}" class="product__option-desc"></a>
                                            {/if}
                                        </p>
                                    </td>
                                    <td>
                                        <span class="product__option-value">{$option.value}</span>
                                    </td>
                                </tr>
                            {/foreach}
                        </table>
                        {else}
                            <p class="product_option-empty">Характеристики не указаны</p>
                        {/if}
                    </div>
                    <div id="prod_rev" class="product__dop-content-item">
                        {'!TicketComments' | snippet : [
                            'allowGuest' => '1'
                        ]}
                    </div>
                    <div id="prod_comment" class="product__dop-content-item">
                        <p>Нужно описание функционала "Коментарии". И чем они отличаются от отзывов?</p>
                    </div>
                    <div id="prod_vision" class="product__dop-content-item">
                        <p>Нужно описание функционала "Обзоры"</p>
                    </div>
                    <div id="prod_quest" class="product__dop-content-item">
                        <p>Нужно описание функционала "Вопрос ответ"</p>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <div class="callback">
        <div class="container">
            {'@FILE snippets/Sendex.php' | snippet : [
                'id' => '1',
                'tplSubscribeGuest' => '@FILE chanks/forms/subscribe.tpl',
                'tplSubscribeAuth' => '@FILE chanks/forms/subscribe.tpl'
            ]}
        </div>
    </div>

    {var $links = '!msProducts' | snippet : [
        'parents' => '2',
        'limit' => '0',
        'includeThumbs' => 'list',
        'link' => '1',
        'master' => $_modx->resource.id,
        'sortby' => '{"price":"DESC"}',
        'tpl' => '@FILE chanks/product/listItemSlider.tpl'
    ]}
    {if $links ?}
        <div class="container">
            <div class="products">
                <p class="products__title --h1">С данным товаром покупают</p>
                <div id="choice" class="swiper-container">
                    <div class="products__list swiper-wrapper">
                        {$links}
                    </div>
                    <div class="swiper-pagination"></div>
                </div>
                <div class="products__more">
                    <a href="{'2' | url}" class="products__more-btn">Смотреть все товары</a>
                </div>
            </div>
        </div>
    {/if}

{/block}

{block 'popups'}
    {parent}
    <div class="quize" id="quize">
        <div class="quize__content">
            <p class="quize__title">Когда-нибудь будет опрос!</p>
            <a class="quize__close" href="javascript:;"></a>
        </div>
    </div>
{/block}
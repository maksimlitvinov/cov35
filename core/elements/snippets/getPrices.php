<?php
$tpl = $modx->getOption('tpl', $scriptProperties, null);

$pricesDir = MODX_ASSETS_PATH . 'uploads/prices/';
$priceLink = MODX_ASSETS_URL . 'uploads/prices/';

$pdoFetch = $modx->getService('pdoFetch');

$files = scandir($pricesDir);

$prices = array();
foreach ($files as $file) {
    if (is_dir($file)) {continue;}

    // Получаем данные о файле
    $stat = stat($pricesDir . $file);

    // Вычисляем размер файла
    $bytes = '0 байтов';
    if ($stat['size'] >= 1073741824) {
        $bytes = number_format($stat['size'] / 1073741824, 2) . ' Gb';
    }

    elseif ($stat['size'] >= 1048576) {
        $bytes = number_format($stat['size'] / 1048576, 2) . ' Mb';
    }

    elseif ($stat['size'] >= 1024) {
        $bytes = number_format($stat['size'] / 1024, 2) . ' Kb';
    }

    elseif ($stat['size'] > 0) {
        $bytes = $stat['size'] . ' B';
    }

    // Получаем имя файла
    $nameArr = explode('.', $file);
    $fileName = 'Неизвестный файл';
    $msEx = array('xls','xlsx');
    $arr = array('zip','rar','tar','7zip','gz','7z');

    if (!isset($nameArr[1])) {
        $fileName = 'Неизвестный файл';
    }
    else if (in_array($nameArr[1],$msEx)) {
        $fileName = 'Microsoft Excel';
    }
    else if (in_array($nameArr[1], $arr)) {
        $fileName = ucfirst($nameArr[1]) . ' - архив';
    }
    else if ($nameArr[1] == 'csv') {
        $fileName = ucfirst($nameArr[1]) . ' файл';
    }
    else if ($nameArr[1] == 'pdf') {
        $fileName = ucfirst($nameArr[1]) . ' файл';
    }

    $prices[] = array(
        'name' => $fileName,
        'size' => $bytes,
        'link' => $priceLink . $file,
        'date' => date ("d.m.Y", $stat['atime'])
    );
}

if (empty($tpl)) {
    return $prices;
}

$output = '';
foreach ($prices as $price) {
    $output .= $pdoFetch->getChunk($tpl, $price);
}
return $output;
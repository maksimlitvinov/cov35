<?php
/**
 * Сниппет для получения TV по категории
 * Если значение не установленно, то подставится значение по-умолчанию
 * из настроек TV
 * Если не указан tpl, то сниппет вернет массив
 */


$pdoFetch = $modx->getService('pdoFetch');
$id = empty($id) ? $modx->resource->get('id') : $id;
$cat = empty($category) ? 15 : $category;
$tpl = $modx->getOption('tpl', $scriptProperties, null);
$tplEmpty = $modx->getOption('tplEmpty', $scriptProperties, '@INLINE <p class="empty">{$text}</p>');
$textEmpty = $modx->getOption('textEmpty', $scriptProperties, 'Характеристики не указаны');

/*
 * Получаем TV по категории
 */
$q = $modx->newQuery('modTemplateVar');
$q->where(array(
    'category' => $cat
));
$res = $modx->getCollection('modTemplateVar', $q);

$resource = $modx->getObject('modResource', $id);

$tvs = array();
foreach ($res as $tv) {
    $name = $tv->get('name');
    $value = $resource->getTVValue($name);
    $value = empty($value) ? $tv->get('default_text') : $value;
    if (!empty($value)) {
        $tmp = array(
            'name' => $name,
            'caption' => $tv->get('caption'),
            'description' => $tv->get('description'),
            'value' => empty($value) ? $tv->get('default_text') : $value
        );
        if (!empty($tpl)) {
            $tvs[] = $pdoFetch->getChunk($tpl, $tmp);
        }
        else {
            $tvs[] = $tmp;
        }
    }
}

if (!empty($tpl)) {
    if (!empty($tvs)) {
        $tvs = implode('', $tvs);
    } else {
        $tvs = $pdoFetch->getChunk($tplEmpty, array('text' => $textEmpty));
    }
}

return $tvs;
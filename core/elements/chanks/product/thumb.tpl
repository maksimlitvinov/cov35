{var $mainImg = $_modx->resource.id | resource : 'main_img'}
{if $files ?}

    {foreach $files as $file}
        <a href="javascript:;" class="product__base-img-prev-item swiper-slide">
            <img src="{$file['small']}" alt="{$file['name']}" class="product__base-img-prev-item-img">
        </a>
    {/foreach}

{elseif $mainImg ?}

    <a href="javascript:;" class="product__base-img-prev-item swiper-slide">
        <img src="{$mainImg}" alt="{$_modx->resource.pagetitle}" class="product__base-img-prev-item-img">
    </a>

{/if}
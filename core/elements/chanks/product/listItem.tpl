<div class="products__item">
    <a href="{$uri}" class="products__item-link">
        <div class="products__item-block">
            {if $fils ?}
                <img src="{$list}" alt="" class="product__item-img">
            {else}
                {var $prev = $id | resource : 'main_img'}
                <img src="{$prev | phpthumbon : 'w=228&h=228&zc=0&bg=FFFFFF'}" alt="" class="product__item-img">
            {/if}
        </div>
        <div class="products__item-block">
            <p class="products__item-title">{$pagetitle}</p>
            {if $introtext ?}
                <p class="products__item-desc">{$introtext}</p>
            {/if}
        </div>
        <div class="products__item-block --price-block">
            {if $old_price ?}
                {var $sale = ((100 - $price * 100 / $old_price) | limit : '2') | replace : '.' : ''}
                <span class="products__item-sale">- {$sale}%</span>
            {/if}
            <div class="products__item-prices">
                <span class="products__item-price">{$price} р.</span>
                {if $old_price ?}
                    <span class="products__item-price --old">{$old_price} р.</span>
                {/if}
            </div>
        </div>
    </a>
    <form class="products__item-btns ms2_form" method="post">
        <input type="hidden" name="count" value="1">
        <input type="hidden" name="id" value="{$id}">
        <input type="hidden" name="options" value="[]">
        <a href="javascript:;" class="products__item-btn btn">Купить в 1 клик</a>{$btnColor}
        <button class="products__item-btn --red" type="submit" name="ms2_action" value="cart/add">Купить</button>
    </form>
    <p class="products__item-available">наличие:
        {var $available = 'Под заказ'}
        {if $inshop ?}
            {set $available = ($inshop | length) | decl : 'магазин|магазина|магазинов' : true}
        {/if}
        <a class="products__item-available-link" href="javascript:;">{$available}</a>
    </p>
</div>
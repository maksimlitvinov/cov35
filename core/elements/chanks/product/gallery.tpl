{var $mainImg = $_modx->resource.id | resource : 'main_img'}
{if $files ?}

    {foreach $files as $file}
        <img src="{$file['prod']}" alt="{$file['name']}" class="product__base-img-base-img swiper-slide">
    {/foreach}

{elseif $mainImg ?}

    <img src="{$mainImg}" alt="{$_modx->resource.pagetitle}" class="product__base-img-base-img swiper-slide">

{/if}

<form action="[[~[[+pageId]]]]" method="get" class="header__search-form msearch2" id="mse2_form">
    <div class="form-group col-md-10">
        <input type="text" class="header__search-input input-text" name="[[+queryVar]]" autocomplete="off" placeholder="Поиск товаров" value="[[+mse2_query]]" />
        <button type="submit" class="header__search-button"></button>
    </div>
</form>
{if $message ?}
    <p class="{$class}"><b>{$message}</b></p>
{else}
    <p class="callback__text">Подпишись! {$name}</p>

    <form action="" method="post" class="callback__form">
        <input type="hidden" name="sx_action" value="subscribe">
        <input class="callback__form-input need not-ico" type="email" name="email" value="" placeholder="Введите email">
        <input class="btn callback__form-btn" type="submit" name="submit" value="Подписаться">
    </form>
{/if}
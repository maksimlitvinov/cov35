<div class="catsmenu__submenu" data-parent="{$id}">
    <div class="catsmenu__submenu-title">{$menutitle ?: $pagetitle}</div>
    {'!pdoMenu' | snippet : [
        'parents' => $id,
        'where' => '{"class_key":"msCategory"}',
        'level' => '2',
        'firstClass' => '',
        'lastClass' => '',
        'parentClass' => 'parent',
        'outerClass' => 'catsmenu__submenu-list',
        'rowClass' => 'catsmenu__submenu-item',
        'tplOuter' => '@INLINE <ul {$classes}>{$wrapper}</ul>',
        'tpl' => '@INLINE <li {$classes}><a href="{$link}" class="catsmenu__submenu-item-link" {$attributes}>{$menutitle ?: $pagetitle}</a>{$wrapper}</li>'
    ]}
</div>
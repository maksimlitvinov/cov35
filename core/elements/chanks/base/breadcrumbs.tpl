{'!pdoCrumbs' | snippet : [
    'outputSeparator' => '<li class="breadcrumbs__item">&nbsp;/&nbsp;</li>',
    'tpl' => '@INLINE <li class="breadcrumbs__item"><a href="{$link}" class="breadcrumbs__link">{$menutitle}</a></li>',
    'tplCurrent' => '@INLINE <li class="breadcrumbs__item">{$menutitle}</li>',
    'tplWrapper' => '@INLINE <ul class="breadcrumbs">{$output}</ul>',
    'showHome' => '1'
]}

{if $doc ?}
    {var $banners = ($doc | resource : 'indexBanners') | fromJSON}
{else}
    {var $banners = $_modx->resource.indexBanners | fromJSON}
{/if}
{if $banners | length > 0 ?}
    <div class="index__banners">
        <div class="container">
            <div class="banners">
                {foreach $banners as $banner}
                    <div class="banners__item">
                        <a href="{$banner.link | url}" class="banners__item-link">
                            <img src="{($banner.image | getPath) | phpthumbon : 'w=555&h=255&zc=1&q=60'}" alt="Баннер" class="banners__item-img">
                        </a>
                    </div>
                {/foreach}
            </div>
        </div>
    </div>
{/if}
<div class="catalog__filter-item-block-wrap">
    <input type="checkbox" {$checked} {$disabled} name="{$filter_key}" value="{$value}" class="catalog__filter-item-checkbox" id="mse2_{$table}{$delimeter}{$filter}_{$idx}">
    <label class="catalog__filter-item-checkbox-lb {$disabled}" for="mse2_{$table}{$delimeter}{$filter}_{$idx}">
        {$title}
        {if $num ?}
            ({$num})
        {/if}
    </label>
</div>
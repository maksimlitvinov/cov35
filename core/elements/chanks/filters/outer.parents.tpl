<div class="catalog__filter-item" id="mse2_{$table}{$delimeter}{$filter}">
    <a href="javascript:;" class="catalog__filter-item-title">
        <i class="catalog__filter-item-title-icon"></i>
        <span class="catalog__filter-item-title-text">Производитель</span>
    </a>
    <div class="catalog__filter-item-block">
        {$rows}
    </div>
</div>
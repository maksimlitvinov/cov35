<div id="mse2_mfilter" class="catalog">
    <div class="catalog__filter">
        <form class="catalog__filter-form" action="{$_modx->resource.uri}" method="post" id="mse2_filters">
            {$filters}
        </form>
    </div>
    <div class="catalog__products">
        <div class="products">
            <div id="mse2_results" class="products__list">
                {$results}
            </div>

            <div class="products__more">
                <button class="products__more-btn mse2_pag">Показать еще</button>
                <div class="mse2_pagination">
                    {$_modx->getPlaceholder('page.nav')}
                </div>
            </div>
        </div>
    </div>
</div>
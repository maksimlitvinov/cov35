<?php

class Search
{
    /** @var modX $modx */
    private $modx;
    /** @var mSearch2 $mse2 */
    private $mse2;

    /**
     * Search constructor.
     * @param modX $modx
     * @param array $config
     */
    public function __construct(modX $modx, $config = array())
    {
        $this->modx = $modx;
        $modx->loadClass('msearch2', MODX_CORE_PATH . 'components/msearch2/model/msearch2/', false, true);
        $mse2 = new mSearch2($modx);
        $mse2->initialize($modx->context->key);
    }

    /**
     * @return array
     */
    public function getQueries()
    {
        $where = array(
            'found:>' => 0
        );
        if (isset($_REQUEST['query']) && !empty($_REQUEST['query'])) {
            $where['query:LIKE'] = '%' . $_REQUEST['query'] . '%';
        }

        $q = $this->modx->newQuery('mseQuery');
        $q->select('query,quantity,found');
        $q->where($where);
        $q->sortby('quantity', 'DESC');
        $q->limit(6);

        $res = array();
        if ($q->prepare() && $q->stmt->execute()) {
            $res = $q->stmt->fetchAll(PDO::FETCH_ASSOC);
        }
        return $this->success($res, $this->modx->getCount('mseQuery', $q));
    }

    /**
     * @param $data
     * @param int $count
     * @param string $message
     * @return array
     */
    protected function success($data, $count = 0, $message = '')
    {
        return array(
            'status' => 'success',
            'data' => $data,
            'message' => $message,
            'count' => $count
        );
    }

    /**
     * @param $data
     * @param string $message
     * @return array
     */
    protected function error($data, $message = '')
    {
        return array(
            'status' => 'error',
            'data' => $data,
            'message' => $message
        );
    }
}
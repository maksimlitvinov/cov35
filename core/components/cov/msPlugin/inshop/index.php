<?php
return array(
    'map' => array(
        'msProductData' => require_once 'msproductdata.inc.php',
    ),
    'manager' => array(
        'msProductData' => MODX_ASSETS_URL . 'components/cov/msPlugin/inshop/js/msproductdata.js',
    ),
);
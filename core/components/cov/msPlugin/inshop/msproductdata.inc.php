<?php
return array(
    'fields' => array(
        'inshop' => null,
    ),
    'fieldMeta' => array(
        'inshop' => array(
            'dbtype' => 'text',
            'phptype' => 'json',
            'null' => true,
            'default' => null,
        ),
    ),
);
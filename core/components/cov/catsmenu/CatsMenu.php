<?php

class CatsMenu
{
    /** @var modX $modx */
    private $modx;
    /** @var pdoFetch $pdoFetch */
    private $pdoFetch;

    public function __construct(modX $modx, $config = array())
    {
        $this->modx = $modx;
        $this->pdoFetch = $modx->getService('pdoFetch');
    }

    public function getSubmenuItems()
    {
        if (!isset($_REQUEST['id']) || empty($_REQUEST['id'])) {
            return $this->error(array(), 'Не указан ID ресурса');
        }
        $resource = $this->pdoFetch->getArray('msCategory', $_REQUEST['id']);
        return $this->success($this->pdoFetch->getChunk('@FILE chanks/menu/catsmenu.tpl', $resource));
    }

    /**
     * @param $data
     * @param int $count
     * @param string $message
     * @return array
     */
    protected function success($data, $count = 0, $message = '')
    {
        return array(
            'status' => 'success',
            'data' => $data,
            'message' => $message,
            'count' => $count
        );
    }

    /**
     * @param $data
     * @param string $message
     * @return array
     */
    protected function error($data, $message = '')
    {
        return array(
            'status' => 'error',
            'data' => $data,
            'message' => $message
        );
    }
}
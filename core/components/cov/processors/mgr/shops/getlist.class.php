<?php
class msPluginGetListProcessor extends modObjectGetListProcessor
{

    public $classKey = 'modResource';
    public $defaultSortField = 'id';
    public $defaultSortDirection = 'ASC';
    public $fromRes = 33173;

    /**
     * @return bool|null|string
     */
    public function initialize()
    {
        if (!$this->modx->hasPermission($this->permission)) {
            return $this->modx->lexicon('access_denied');
        }

        if (!empty($this->getProperty('parent'))) {
            $this->fromRes = $this->getProperty('parent');
        }

        return parent::initialize();
    }

    /**
     * Can be used to adjust the query prior to the COUNT statement
     *
     * @param xPDOQuery $c
     * @return xPDOQuery
     */
    public function prepareQueryBeforeCount(xPDOQuery $c) {
        $c->where(array(
            'parent' => $this->fromRes
        ));
        return $c;
    }

    /**
     * @param xPDOObject $object
     *
     * @return array
     */
    public function prepareRow(xPDOObject $object)
    {
        $data = array();
        if ($this->getProperty('combo')) {
            $data = array(
                'id' => $object->get('id'),
                'pagetitle' => $object->get('pagetitle'),
            );
        } else {
            $data = $object->toArray();
        }

        return $data;
    }

}
return 'msPluginGetListProcessor';